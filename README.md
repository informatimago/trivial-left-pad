## Introduction

trivial-left-pad is the Common Lisp version of the extremely popular
node.js left-pad package. It is designed to have as similar an API to
node's left-pad as possible while still remaining idiomatic Lisp. For
more information on the principles behind this project, see
design-philosophy.md

## Installation

trivial-left-pad is not on Quicklisp. To make Quicklisp recognize
trivial-left-pad, create a link to this directory in Quicklisp's
local-projects. By default, the path to local-projects is:

```sh
~/quicklisp/local-projects
```

Quicklisp is the recommended way to use ASDF systems. Any other method
of installation is currently unsupported. If you need to install
Quicklisp, [read this link](https://www.quicklisp.org/).

## Loading

You should be able to load trivial-left-pad from your REPL with:

```lisp
(ql:quickload :trivial-left-pad)
```

You will also be able to use it as an ASDF dependency from any project
also in local-projects. To add it as an ASDF dependency, add this line
to your asdf:defsystem expression:

```lisp
:depends-on (:trivial-left-pad)
```

If the depends-on line already exists, don't add another depends-on
line. Instead, append it to the existing list like in this example:

```lisp
:depends-on (:some-other-package :trivial-left-pad)
```

You will also should add it to your defpackage expression, which could
be in package.lisp or could be in another file. You need to append it
to your :use list like in this example:

```lisp
(:use #:cl #:trivial-left-pad)
```

After this, you should be able to call left-pad from your Lisp
program.

## Testing

To test trivial-left-pad, run the following command from your REPL:

```lisp
(asdf:test-system :trivial-left-pad)
```

## Example usage

```lisp
(left-pad "foobar" 10) ; "    foobar"
(left-pad "Hello, world!" 15 #\_) ; "__Hello, world!"
```

## Differences with node's left-pad

While compatibility with node's left-pad is a goal, there are some
differences that should be kept in mind:

- node's left-pad takes a string of length one as its character
  argument. This left-pad takes a character because Common Lisp has a
  character data type. The decision was made to be idiomatic here
  rather than fully API-compatible.
- This left-pad checks the types of the length and character
  arguments. Any undefined behavior that you might rely on in node's
  left-pad will not work here.
- This left-pad is written in Common Lisp, not JavaScript, and is
  designed to be used with Common Lisp, not JavaScript.
- Using Common Lisp package naming conventions, trivial-left-pad is
  prefixed with "trivial-" because it is trivial.
