(defpackage #:trivial-left-pad-test
  (:use #:cl #:prove #:trivial-left-pad))

(in-package #:trivial-left-pad-test)

(plan 27)

(is (left-pad "Hello world!" 15) "   Hello world!")
(is (left-pad "Hello world!" 5) "Hello world!")
(is (left-pad "foo" 9 #\f) "fffffffoo")
(is (left-pad "foo" 5 #\_) "__foo")
(is (left-pad "foo" 5 #\Space) "  foo")
(is (left-pad "" 10 #\λ) "λλλλλλλλλλ")
(is (left-pad #() 5 #\#) "###()")
(is (left-pad (cons 1 2) 8) " (1 . 2)")
(is (left-pad 42 42) "                                        42")
(is-error (left-pad "foo" t) 'type-error)
(is-error (left-pad "foo" -1) 'type-error)

(is (left-pad #() 5 #\#) "###()")
(is (left-pad "foo" 9 "f") "fffffffoo")
(is (left-pad "foo" 9 "f") "fffffffoo")
(is (left-pad "foo" 5 "_") "__foo")
(is (left-pad "foo" 5 " ") "  foo")
(is (left-pad ""   10 " ") "λλλλλλλλλλ")

(is (left-pad '|foo| 9 'f)   "FFFFFFfoo")
(is (left-pad '|foo| 9 '|f|) "fffffffoo")
(is (left-pad "foo" 5 '_)   "__foo")
(is (left-pad "foo" 5 '\ )  "  foo")
(is (left-pad ""   10 '| |) "λλλλλλλλλλ")
(is (left-pad '||  10 '| |) "λλλλλλλλλλ")
(is (left-pad '||  10 '||)  "λλλλλλλλλλ")
(is (left-pad '||  10 "")   "λλλλλλλλλλ")

(is (left-pad "foo"  9 "-=#")   "-=#-=#foo")
(is (left-pad "foo" 11 "-=#") "=#-=#-=#foo")

(finalize)
