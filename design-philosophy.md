## Modularity

> Well-designed computational systems, like well-designed automobiles
> or nuclear reactors, are designed in a modular manner, so that the
> parts can be constructed, replaced, and debugged separately.
>
> Harold Abelson and Gerald Jay Sussman, with Julie Sussman,
> *Structure and Interpretation of Computer Programs* (1996)

It is a commonly known fact in software design that good software is
very modular. Programming should consist of the composition of small
functions, which is a task that Lisp excels at. Thus, it is no
surprise that the Common Lisp ecosystem is one of the first ecosystems
to have a port of the increasingly popular left-pad package and the
new design philosophy that it exemplifies.

The Unix philosophy is a good example of modular thinking. In the
traditional Unix way of doing things, there are small programs that
"do one thing and do it well", which are then composed together. They
speak a common interface, making interactive use of the computer
through the terminal a very effective. Shell scripting, which relies
on this aspect of the Unix philosophy, has been and continues to be
very popular.

In the node.js ecosystem, the node NPM package manager has one of the
largest repositories of free and open source software (FOSS). Many in
that community take this philosophy of modularity to what some see as
an extreme. What is more composable than having each function live in
its own module? left-pad is an example of this new kind of modularity
and in order for other languages to remain competitive, the new best
practices need to be ported to the Common Lisp ecosystem. What better
way to start it then with an API-compatible equivalent for the package
that exemplifies this movement?

## Adaptability

> Lisp is the second-oldest high-level programming language in
> widespread use today; only Fortran is older (by one year).
>
> "Lisp (programming language)", Wikipedia

Adaptability is one of the main reasons for Lisp's remarkably long
lifespan as a programming language. When new paradigms of programming
are invented, it is easy to extend Lisp to handle them. One famous
example is the Common Lisp Object System (CLOS), which adds object
oriented programming (OOP) to Common Lisp in a very elegant and
powerful way. Even though Lisp is considerably older than object
oriented programming, Common Lisp manages to have a spectacular object
system. This amazing ability for this language to be elegantly
extended into new programming paradigms is one of the most important
features of Lisp.

Object oriented programming is now old and widely supported by many
popular programming languages, with its techniques well
understood. Still, one must wonder what new paradigms are right around
the corner that Lisp must be able to learn. If OOP existed to help
with managing large amounts of code, then surely new paradigms are
evolving right now to handle a world with more FOSS than ever
before. Programmers need to always be on the look out for new
disruptive paradigm shifts in technology.

One of the new developments in programming practice that Lisp must
adapt to is the extreme modularity. Thanks to language package
managers automatically resolving dependencies, the cost of having
dependencies has been greatly reduced. This means that languages can
go to new levels of modularity, shifting the calculus in the best
practices for software design, as noted above. Code reuse, modularity,
and composition have undergone a major paradigm shift and Lisp must
keep up with it.

## A new paradigm?

> The new best practices need to be ported to the Common Lisp
> ecosystem.
>
> design-philosophy.md

left-pad is the perfect example of the new frontier of software
engineering. It's a small, easily composable function offered as an
individual package. Using package managers, it can be trivially
imported into other programs so that no one ever has to write the same
code twice, and everyone can focus only on solving new problems. These
micro-packages can be individually tested, and optimized, and handle
far more edge cases than if everyone were to re-implement left-pad
every time it is needed.

Instead, the upstream can handle all issues, and the end user can
treat the upstream package like a black box, assuming that everything
is handled upstream and that nothing will go wrong upstream. It is
extremely unlikely that upstream will break things, and this frees the
programmer to focus on innovating and moving fast.

If this is the new competitive environment in which Common Lisp finds
itself, it is up to the trivial-left-pad package to port the extremely
popular and critically important left-pad package to the Common Lisp
ecosystem so that Common Lisp does not fall behind JavaScript. It is
imperative that Lisp remain competitive, and adaptable, and able to
keep up with current best practices.

trivial-left-pad is not a translation of left-pad into
JavaScript. Common Lisp is a much larger language and has its own
idioms and a very large standard library. Thus, the intent is to stay
as API-compatible as possible while still remaining idiomatic
Lisp. There are some differences. For more information, see README.md
