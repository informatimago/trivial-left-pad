;;;; the LEFT-PAD function

;;;; Copyright (c) 2016 Michael Babich
;;;;
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package #:trivial-left-pad)

;;;; The functions provided in this file have been designed to be
;;;; mostly equivalent to the functions provided in node's
;;;; left-pad. For more information, see README.md

;;; This function has been designed to be mostly API compatible with
;;; the leftpad function. For more information, see README.md
(defun left-pad (string length &optional padding)
  "Pads the left of string to be length long. It inserts character in
front. If character is nil, space is inserted. If string is not a
string, it is converted to a string."
  ;; Nonsensical values should be caught here.
  (check-type length (integer 0 *))
  (check-type padding string-designator)
  ;; This uses a FORMAT string to accomplish the desired left pad
  ;; behavior. Although ~A is common, these optional prefixes are
  ;; not. v is used to read in the first two format-arguments as
  ;; customized parameters of the prefix instead of hardcoding fixed
  ;; behavior in or dynamically generating a format string. @ is used
  ;; to pad on the left instead of the default right-pad behavior.
  ;;
  ;; ~mincol,colinc,minpad,padchar@A is the API for a left-padded A
  ;; (Aesthetic) format. left-pad doesn't customize colinc or minpad
  ;; so they are kept at their default values of 1 and 0. A future
  ;; version of this function could accept them as additional optional
  ;; arguments, or this function could be converted to use keyword
  ;; arguments for maximum flexibility. mincol (minimum columns) is
  ;; equivalent to padding length.
  ;;
  ;; For more information, see section 22.3 of the Hyperspec,
  ;; Formatted Output.
  (let* ((string  (typecase string
                    (string-designator (string string))
                    (t                 (format nil "~A" string))))
         (padding (string (or padding " ")))
         (padding (if (zerop (length padding))
                      " "
                      padding)))
   (if (= 1 (length padding))
       (format nil "~v,,,v@A" length (character padding) string)
       (multiple-value-bind (repeat remainder)
           (truncate (max 0 (- length (length string)))
                     (length padding))
         (let ((*print-circle* nil))
           (format nil "~A~v{~A~:*~}~A"
                   (subseq padding (- (length padding) remainder))
                   repeat (list padding)
                   string))))))
