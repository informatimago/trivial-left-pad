(asdf:defsystem #:trivial-left-pad
  :serial t
  :description "Ports the functionality of the very popular left-pad from npm."
  :author "Michael Babich"
  :license "MIT"
  :depends-on (:alexandria)
  :components ((:file "package")
               (:file "trivial-left-pad"))
  :in-order-to ((test-op (test-op trivial-left-pad-test))))

(asdf:defsystem #:trivial-left-pad-test
    :description "Tests trivial-left-pad."
    :author "Michael Babich"
    :license "MIT"
    :depends-on (:trivial-left-pad
                 :prove)
    :defsystem-depends-on (:prove-asdf)
    :components ((:module "t"
                  :components
                  ((:test-file "test-left-pad"))))
    :perform (test-op :after (op c)
                      (funcall (intern #.(string :run) :prove) c)))
